FROM node:18.16.0-alpine3.17

WORKDIR /app

COPY . .

RUN npm ci

RUN npm run build

EXPOSE 8001

CMD ["npm", "start"]