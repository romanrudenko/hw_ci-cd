import request from 'supertest';
import app from '../src/index';

describe('Express server', () => {
  it('responds with the current date', async () => {
    const response = await request(app).get('/').expect(200);

    expect(response.text).toContain('Hello, this is homework for CI/CD');
  });
});
